# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SortType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=70, verbose_name='Name')),
                ('order', models.IntegerField(default=0)),
                ('sortable_fields', models.TextField(default=b'name', verbose_name='Sortable fields')),
            ],
            options={
                'verbose_name': 'Sort type',
                'verbose_name_plural': 'Sort types',
            },
            bases=(models.Model,),
        ),
    ]
