# coding: utf-8
from django import template
import requests
import re
from core.models import site_options


register = template.Library()


@register.inclusion_tag('catalog/menu_tree.html', takes_context=True)
def menu_tree(context, current_category):
    children = []
    for c in context['categories']:
        if c['parent'] == current_category['id']:
            children.append(c)
    context['children'] = children
    return context


@register.inclusion_tag('catalog/menu.html', takes_context=True)
def menu(context):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}categories/'.format(api_entrypoint))
    result = r.json()
    context['categories'] = result['categories']
    return context


@register.filter()
def load_images(text):
    shop_domain = site_options.main_site_domain
    image_regex = re.compile('src="(.+?)"')
    links = image_regex.findall(text)
    new_links = []
    for link in links:
        if link.startswith('/'):
            new_links.append({
                'new': shop_domain + link,
                'old': link
            })
    for link in new_links:
        text = text.replace(link['old'], link['new'])
    return text
