# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class SortType(models.Model):
    """Sort type for categories and product lists.

    name
        Sort type name.

    order
        Sort type position in dropdown list.

    sortable_fields
        The comma separated list of fields.
    """
    name = models.CharField(_(u"Name"), max_length=70)
    order = models.IntegerField(default=0)
    sortable_fields = models.TextField(_(u"Sortable fields"), default='name')

    class Meta:
        verbose_name = _(u'Sort type')
        verbose_name_plural = _(u'Sort types')
        ordering = ['order']

    def __unicode__(self):
        return self.name
