# coding: utf-8
from django.contrib import admin
from suit.admin import SortableModelAdmin
from .models import SortType


class SortTypeAdmin(SortableModelAdmin):
    list_display = ('name', 'sortable_fields')
    search_fields = ['name']
    sortable = 'order'

admin.site.register(SortType, SortTypeAdmin)
