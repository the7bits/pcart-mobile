# coding: utf-8
from django.views.decorators.cache import cache_page
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, Http404
from django.views.decorators.csrf import ensure_csrf_cookie
import requests
from core.models import site_options
from .models import SortType
from django.core.urlresolvers import reverse
import urllib


@cache_page(60*15)
@ensure_csrf_cookie
def category_view(
        request, category_slug, template_name='catalog/category.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}category/{1}/'.format(
        api_entrypoint, category_slug))
    if r.status_code == 404:
        raise Http404
    category = r.json()

    filter_str = request.GET.copy()
    if 'car' in filter_str:
        del filter_str['car']
    if filter_str:
        r = requests.get(
            '{0}products/{1}/'.format(api_entrypoint, category_slug),
            params=filter_str)
    else:
        r = requests.get(
            '{0}products/{1}/'.format(api_entrypoint, category_slug))
    products = r.json()
    if 'detail' in products and products['detail'] == u'Not found':
        raise Http404
    if products['next']:
        next_url = '%s?%s' % (request.path, products['next'].split('?')[1])
    else:
        next_url = None
    if products['previous']:
        previous_url = '%s?%s' % (
            request.path, products['previous'].split('?')[1])
    else:
        previous_url = None
    current = filter_str['sorting'] if 'sorting' in filter_str else ''
    return render_to_response(
        template_name,
        RequestContext(request, {
            'category': category,
            'products': products['results'],
            'count': products['count'],
            'previous': previous_url,
            'next': next_url,
            'sort_types': SortType.objects.all(),
            'current': current,
        }))


@cache_page(60*15)
@ensure_csrf_cookie
def filters_view(request, category_slug, template_name='catalog/filters.html'):
    if request.POST:
        get_params = []
        api_entrypoint = site_options.api_entrypoint
        r = requests.get('{0}filters/{1}/'.format(
            api_entrypoint, category_slug))
        filters = r.json()
        filters_dict = {}
        for f in filters:
            if f['type'] == 20:
                filters_dict[f['identificator']] = f
        for key, values in dict(request.POST).iteritems():
            if key != 'csrfmiddlewaretoken':
                if key in filters_dict:
                    from math import trunc, ceil
                    min_value = str(int(round(filters_dict[key]['min_value'])))
                    max_value = str(int(round(filters_dict[key]['max_value'])))
                    try:
                        cell_from = trunc(
                            float(values[0].replace(",", ".")))
                        cell_to = int(ceil(
                            float(values[1].replace(",", "."))))
                        values[0] = str(cell_from)
                        values[1] = str(cell_to)
                    except:
                        values[0] = "0"
                        values[1] = "0"
                    if min_value != values[0] or max_value != values[1]:
                        params = '..'
                        if min_value != values[0]:
                            params = values[0] + params
                        if max_value != values[1]:
                            params = params + values[1]
                        get_params.append('%s=%s' % (key, params))
                else:
                    get_params.append('%s=%s' % (key, ','.join(values)))
        return HttpResponseRedirect(
            reverse(
                "category",
                kwargs={
                    'category_slug': category_slug
                }) + '?%s' % '&'.join(get_params)
        )
    else:
        filter_params = request.GET.dict()
        api_entrypoint = site_options.api_entrypoint
        r = requests.get('{0}filters/{1}/'.format(
            api_entrypoint, category_slug))
        filters = r.json()
        if filter_params:
            for item in filters:
                identificator = item['identificator']
                if identificator in filter_params:
                    params_list = filter_params[identificator].split(',')
                    if item['type'] == 5:
                        for fo in item['filteroption_set']:
                            if fo['identificator'] in params_list:
                                fo['selected'] = True
                    elif item['type'] == 10 or item['type'] == 0:
                        for fo in item['filteroption_set']:
                            if fo['identificator'] in params_list:
                                fo['checked'] = True
                    elif item['type'] == 20:
                        splitted_avg = filter_params[identificator].split('..')
                        min_value = splitted_avg[0]
                        max_value = splitted_avg[1]
                        if min_value and\
                                (float(min_value) >= item['min_value']):
                            item['min_value_init'] = min_value
                        if max_value and\
                                (float(max_value) <= item['max_value']):
                            item['max_value_init'] = max_value
        r = requests.get('{0}category/{1}/'.format(
            api_entrypoint, category_slug))
        category = r.json()
        if 'detail' in category and category['detail'] == u'Not found':
            raise Http404
        return render_to_response(
            template_name,
            RequestContext(request, {
                'filters': filters,
                'category': category,
                'page_type': 'filters'
            }))


@cache_page(60*15)
@ensure_csrf_cookie
def product_view(
        request, product_slug, template_name='catalog/product.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}product/{1}/'.format(
        api_entrypoint, product_slug))
    if r.status_code == 404:
        raise Http404
    product = r.json()
    r = requests.get('{0}product-variants/{1}/'.format(
        api_entrypoint, product_slug))
    variants = r.json()
    if 'detail' in product and product['detail'] == u'Not found':
        raise Http404
    return render_to_response(
        template_name,
        RequestContext(request, {
            'product': product,
            'variants': variants,
            'category': product['category']
        }))


@cache_page(60*15)
@ensure_csrf_cookie
def product_images_view(
        request, product_slug,
        template_name='catalog/product-all-images.html'):
    return product_view(request, product_slug, template_name)


@cache_page(60*15)
def product_reviews_view(
        request, product_slug, template_name='catalog/reviews.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}reviews/{1}/'.format(
        api_entrypoint, product_slug))
    reviews = r.json()
    r = requests.get('{0}product/{1}/'.format(
        api_entrypoint, product_slug))
    product = r.json()
    return render_to_response(
        template_name,
        RequestContext(request, {
            'reviews': reviews,
            'product': product,
            'category': product['category']
        }))


@ensure_csrf_cookie
def product_search_view(request, template_name='catalog/search.html'):
    if 'q' in request.GET and request.GET['q'] != '':
        api_entrypoint = site_options.api_entrypoint
        get_params = []
        for key, values in dict(request.GET).iteritems():
            get_params.append(
                '%s=%s' % (key, urllib.quote(','.join(values).encode('utf8')))
            )

        r = requests.get(
            '{0}search/?{1}'.format(
                api_entrypoint,
                '&'.join(get_params)
            ),
        )
        products = r.json()
        if products['next']:
            next_url = '%s?%s' % (request.path, products['next'].split('?')[1])
        else:
            next_url = None
        if products['previous']:
            previous_url = '%s?%s' % (
                request.path, products['previous'].split('?')[1])
        else:
            previous_url = None
        data = {
            'products': products['results'],
            'count': products['count'],
            'previous': previous_url,
            'next': next_url,
            'query': request.GET['q'] if 'q' in request.GET else None
        }
    else:
        data = {}
    return render_to_response(
        template_name,
        RequestContext(request, data))
