from django.shortcuts import HttpResponse
from django.contrib.sites.models import get_current_site
from core.models import site_options
from django.conf import settings
from django.template.response import TemplateResponse
from datetime import datetime
import requests

def get_domain(request):
    protocol = 'https' if request.is_secure() else 'http'
    req_site = get_current_site(request)
    return '%s://%s' % (protocol, req_site.domain)

def index(
        request,
        template_name='sitemap_index.xml', mimetype='application/xml'):
    """ Generation index sitemap
    """
    sites = []

    sitemap_url = '/sitemap/pages.xml'
    absolute_url = '%s%s' % (get_domain(request), sitemap_url)
    sites.append(absolute_url)

    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}categories/'.format(api_entrypoint))
    result = r.json()['categories']

    for i in result:
        sitemap_url = '/sitemap/category/c/' + i['slug'] + '.xml'
        absolute_url = '%s%s' % (get_domain(request), sitemap_url)
        sites.append(absolute_url)

    return TemplateResponse(request, template_name, {'sitemaps': sites},
                            content_type=mimetype)

def products_sitemap(request, category_slug, template_name='sitemap.xml', mimetype='application/xml'):
    api_entrypoint = site_options.api_entrypoint

    r = requests.get(
        '{0}products-slug/{1}/'.format(api_entrypoint, category_slug))

    products = r.json()

    protocol = 'https' if request.is_secure() else 'http'
    req_site = get_current_site(request)
    urlset = [{
        'location': '%s/c/%s/' % (get_domain(request), category_slug),
        'lastmod': datetime.now(),
        'changefreq': 'weekly',
        'priority': '0.5'
    }]
    for product in products:
        urlset.append({
            'location': '%s/p/%s/' % (get_domain(request), product['slug']),
            'lastmod': datetime.strptime(product['create'], "%Y-%m-%d"),
            'changefreq': 'weekly',
            'priority': '0.5'
        })

    return TemplateResponse(request, template_name, {'urlset': urlset},
                            content_type=mimetype)

def pages_sitemap(request, template_name='sitemap.xml', mimetype='application/xml'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}pages/'.format(api_entrypoint))
    pages = r.json()
    protocol = 'https' if request.is_secure() else 'http'
    req_site = get_current_site(request)
    urlset = []
    for page in pages:
        urlset.append({
            'location': '%s/page/%s/' % (get_domain(request), page['slug']),
            'lastmod': datetime.now(),
            'changefreq': 'weekly',
            'priority': '0.5'
        })
    return TemplateResponse(request, template_name, {'urlset': urlset},
                            content_type=mimetype)
