from django import forms
from django.utils.translation import ugettext_lazy as _


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=200,
        required=True,
        label=_(u'Username')
    )
    password = forms.CharField(
        max_length=200,
        widget=forms.PasswordInput,
        label=_(u'Password')
    )
