from django.conf.urls import patterns, include, url
from rootfiles.models import File

proxies = []
files = File.objects.all()
for _file in files:
    proxies.append(
        url('^' + _file.name + '$', 'serve', {'filename': _file.name})
        )

urlpatterns = patterns('rootfiles.views', *proxies)

urlpatterns += patterns(
    'core.views',
    url(r'^robots.txt$', 'robotstxt_view', name='robotstxt'),
    url(r'^humans.txt$', 'humanstxt_view', name='humanstxt'),
    url(r'^page/(?P<page_slug>[-\w]+)/$', 'page_view', name='page'),
    url(r'^login/$', 'login', name='login'),
    url(r'^clear-cache$', 'clear_cache', name='clear_cache'),
    url(r'^restart-server$', 'restart_engine', name='restart_engine'),
    url(r'^$', 'index_view', name='index'),
)

urlpatterns += patterns(
    'catalog.views',
    url(r'^c/(?P<category_slug>[-\w]+)/$', 'category_view', name='category'),
    url(r'^f/(?P<category_slug>[-\w]+)/$', 'filters_view', name='filters'),
    url(r'^p/(?P<product_slug>[-\w]+)/$', 'product_view', name='product'),
    url(
        r'^p-images/(?P<product_slug>[-\w]+)/$',
        'product_images_view', name='product_images'),
    url(
        r'^p-reviews/(?P<product_slug>[-\w]+)/$',
        'product_reviews_view', name='product_reviews'),
    url(r'^search/$', 'product_search_view', name='search'),
)

urlpatterns += patterns(
    'cart.views',
    url(r'^cart/$', 'cart_view', name='cart'),
    url(r'^buy-product/$', 'buy_product', name='buy_product'),
    url(
        r'^remove-product/$',
        'remove_product',
        name='remove_product_from_order'
    ),
    url(
        r'^change-amount/$',
        'change_cart_item_amount',
        name='change_cart_item_amount'
    ),
)

urlpatterns += patterns(
    'checkout.views',
    url(r'^checkout/$', 'checkout', name='checkout'),
    url(r'^thank-you/$', 'thank_you', name='thank_you_page'),
    url(r'^check-voucher/$', 'check_voucher', name='check_voucher'),
    url(
        r'^load-shipping-form-fields/$',
        'load_shipping_form_fields',
        name='load_shipping_form_fields'
    )
)

urlpatterns += patterns(
    '',
    # url(r'^login/$', 'django.contrib.auth.views.login', {
    #     'template_name': 'login.html'
    #     }, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {
        'next_page': '/'
        }, name='logout'),
)

# Sitemaps
urlpatterns += patterns(
    'core.sitemap',
    url(r'^sitemap\.xml$', 'index'),
    url(
        r'^sitemap/category/c/(?P<category_slug>.+)\.xml$',
        'products_sitemap'),
    url(r'^sitemap/pages\.xml$', 'pages_sitemap'),
)

urlpatterns += patterns(
    'sbits_pages.views',
    url(
        r'^info/categories/$',
        'sbits_pages_categories_view',
        name='sp_categories_list'),
    url(
        r'^info/(?P<category_slug>[-\w]+)/$',
        'sbits_pages_category_view',
        name='sp_category'),
    url(
        r'^info/(?P<path>[-\w]+)/(?P<news_slug>[-\w]+)/$',
        'sbits_pages_news_view',
        name='sp_news'),
)

urlpatterns += patterns(
        'product_lists.views',
        url(
            r'^product-lists/$',
            'get_product_lists_page',
            name='get_product_lists'),
        url(
            r'^product-lists/(?P<page_slug>[-\w]+)/$',
            'get_product_list',
            name='get_product_list'),
    )
