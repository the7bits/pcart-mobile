# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=70, verbose_name='Name')),
                ('code', models.CharField(unique=True, max_length=10, verbose_name='Code')),
                ('abbr', models.CharField(max_length=10, verbose_name='Abbr')),
                ('format_str', models.CharField(default='%(value).2f %(abbr)s', max_length=50, verbose_name='Format string')),
                ('coeffitient', models.FloatField(default=1.0, verbose_name='Coeffitient')),
            ],
            options={
                'verbose_name': 'Currency',
                'verbose_name_plural': 'Currencies',
            },
            bases=(models.Model,),
        ),
    ]
