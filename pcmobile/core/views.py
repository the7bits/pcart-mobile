# coding: utf-8
from django.views.decorators.cache import cache_page
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from django.conf import settings
from django.http import HttpResponseRedirect
from django.http import Http404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from .forms import LoginForm
import requests
import base64


# @cache_page(60*15)
def index_view(request, template_name='index.html'):
    from .models import site_options
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}categories/'.format(api_entrypoint))
    result = r.json()
    return render_to_response(
        template_name,
        RequestContext(request, {
            'categories': result['categories'],
            'meta_title': result['meta_title'],
            'meta_keywords': result['meta_keywords'],
            'meta_description': result['meta_description']
        }))


@ensure_csrf_cookie
def login(request):
    if request.POST:
        form = LoginForm(data=request.POST)
        result = {}
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            from .models import site_options
            api_entrypoint = site_options.api_entrypoint
            r = requests.post(
                '{0}login-user/'.format(api_entrypoint),
                data={
                    'username': username,
                    'password': base64.encodestring(password)
                })
            result = r.json()
            if result['status'] == 'login':
                user, created = User.objects.get_or_create(username=username)
                user.set_password(password)
                user.save()
                auth_user = authenticate(username=username, password=password)
                if auth_user is not None:
                    auth_login(request, auth_user)
                return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
            else:
                form.errors['__all__'] = _(
                    _(u"Your username and password didn't match."
                        " Please try again."))
                result['orders'] = None
                result['customer'] = None
        else:
            result['orders'] = None
            result['customer'] = None
    else:
        form = LoginForm()
        result = {}
        result['orders'] = None
        result['customer'] = None
        if request.user.is_authenticated():
            from .models import site_options
            api_entrypoint = site_options.api_entrypoint
            r = requests.post(
                '{0}orders/'.format(api_entrypoint),
                data={'username': request.user.username}
            )
            result = r.json()
    return render_to_response(
        'login.html',
        RequestContext(request, {
            'form': form,
            'orders': result['orders'],
            'customer': result['customer']
        })
    )


def robotstxt_view(request, template_name='robots.txt'):
    return render_to_response(
        template_name,
        content_type='text/plain')


def humanstxt_view(request, template_name='humans.txt'):
    return render_to_response(
        template_name,
        content_type='text/plain')


def page_view(request, page_slug, template_name='page.html'):
    from .models import site_options
    api_entrypoint = site_options.api_entrypoint
    r = requests.get('{0}page/{1}/'.format(api_entrypoint, page_slug))
    if r.status_code == 404:
        raise Http404()
    result = r.json()
    return render_to_response(
        template_name,
        RequestContext(request, {'page': result}))


def clear_cache(request):
    from core.utils import clear_cache
    clear_cache()
    return HttpResponseRedirect(reverse('site_settings'))


def restart_engine(request):
    from core.utils import restart_engine
    restart_engine()
    return HttpResponseRedirect(reverse('site_settings'))
