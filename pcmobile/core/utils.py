from django.core.cache import cache


def clear_cache():
    """Clears the complete cache.
    """
    # memcached
    try:
        cache._cache.flush_all()
    except AttributeError:
        pass
    else:
        return

    try:
        cache._cache.clear()
    except AttributeError:
        pass
    try:
        cache._expire_info.clear()
    except AttributeError:
        pass


def restart_engine():
    """Restarting engine
    """
    from django.conf import settings
    import subprocess
    NGINX_RESTART_CODE = getattr(settings, 'NGINX_RESTART_CODE', False)
    if NGINX_RESTART_CODE:
        subprocess.call(NGINX_RESTART_CODE, shell=True)
