# coding: utf-8


def main(request):
    from .models import site_options
    from .models import Currency
    # import requests
    shop_domain = site_options.main_site_domain
    currency = site_options.currency
    if currency is not None:
        currency = Currency.objects.get(code=currency)
    # api_entrypoint = site_options.api_entrypoint
    # r = requests.get(
    #     '{0}pages/'.format(api_entrypoint))
    # pages = r.json()

    return {
        'shop_domain': shop_domain,
        'currency': currency,
        'shop_title': site_options.title
        # 'static_pages': pages
    }
