from django import template

register = template.Library()


@register.inclusion_tag('pages_block.html', takes_context=True)
def pages_block(context):
    from core.models import site_options
    import requests
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}pages/'.format(api_entrypoint))
    pages = r.json()
    context['static_pages'] = pages
    r = requests.get('{0}product-lists/'.format(api_entrypoint))
    if r.status_code == 200:
        context['product_lists'] = r.json()
    return context
