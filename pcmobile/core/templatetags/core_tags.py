from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def canonical_links(context):
    from core.models import site_options
    shop_domain = site_options.main_site_domain
    product = context.get('product', None)
    category = context.get('category', None)
    page = context.get('page', None)
    if page:
        context['cannonical_url'] = '%s/page/%s' % (shop_domain, page['slug'])
    elif product:
        context['cannonical_url'] = '%s/%s' % (shop_domain, product['slug'])
    elif category:
        context['cannonical_url'] = '%s/%s' % (shop_domain, category['slug'])
    return ''
