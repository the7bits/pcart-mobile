from django.contrib import admin
from core.models import Currency


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'abbr', 'coeffitient')


admin.site.register(Currency, CurrencyAdmin)
