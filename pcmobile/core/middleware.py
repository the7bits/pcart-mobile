# coding: utf-8
from django.conf import settings
from django.template.loader import render_to_string
from django.template import RequestContext
from django.http import HttpResponse, HttpResponsePermanentRedirect
from urllib import urlencode
import requests
import json


class MaintenanceMiddleware(object):
    def process_request(self, request):
        from core.models import site_options
        api_entrypoint = site_options.api_entrypoint
        r = requests.get('{0}maintenance-messages/'.format(api_entrypoint))
        messages = r.json()
        max_type = 0
        for m in messages:
            if m['type'] > max_type:
                max_type = m['type']

        if '/admin' not in request.path \
                and len(messages):
            if max_type < 15 or request.user.is_superuser:
                request.maintenance_messages = messages
                request.max_maintenance_type = max_type
                return None
            else:
                template = render_to_string('503.html', {
                    'messages': messages
                }, context_instance=RequestContext(request))
                return HttpResponse(template, status=503)
        else:
            if len(messages):
                request.maintenance_messages = messages
                request.max_maintenance_type = max_type
                return None
            else:
                return None


class RedirectToFullSite(object):
    def process_request(self, request):
        from .models import site_options
        api_entrypoint = site_options.api_entrypoint
        if request.path == '/'\
                or request.path.startswith('/step-by-step-form'):
            return HttpResponsePermanentRedirect(site_options.main_site_domain)
        if request.path == '/search/':
            url = '%s/search' % site_options.main_site_domain
            if request.GET:
                url += '?%s' % urlencode(request.GET).replace('%2C', ',')
            return HttpResponsePermanentRedirect(url)
        if request.path == '/login/':
            return HttpResponsePermanentRedirect(
                '%s/login' % site_options.main_site_domain)
        if request.path == '/cart/':
            return HttpResponsePermanentRedirect(
                '%s/checkout' % site_options.main_site_domain)
        if request.path == '/info/categories/':
            return HttpResponsePermanentRedirect(
                '%s/info/' % site_options.main_site_domain)
        stripped_path = request.path.strip('/')
        if stripped_path.startswith('c/') or stripped_path.startswith('p/')\
                or stripped_path.startswith('f/')\
                or stripped_path.startswith('p-reviews/'):
            if request.GET and stripped_path.startswith('c/'):
                params = request.GET.copy()
                if 'page' in params:
                    params['start'] = params['page']
                    del params['page']
                r = requests.get(
                    '{0}products/{1}/'.format(
                        api_entrypoint,
                        stripped_path.split('/')[1]),
                    params=params)
                if r.status_code == 200:
                    response = json.loads(r.content)
                    if 'path' in response:
                        return HttpResponsePermanentRedirect(
                            '%s/%s' % (
                                site_options.main_site_domain,
                                response['path']
                            )
                        )
            else:
                return HttpResponsePermanentRedirect(
                    '%s/%s' % (
                        site_options.main_site_domain,
                        stripped_path.split('/')[1]
                    )
                )
        elif stripped_path.startswith('page/'):
            return HttpResponsePermanentRedirect(
                '%s/%s' % (
                    site_options.main_site_domain,
                    stripped_path
                )
            )
        elif stripped_path.startswith('info/'):
            url = '%s/%s' % (
                site_options.main_site_domain,
                stripped_path
            )
            print url, len(stripped_path.split('/'))
            if len(stripped_path.split('/')) > 2:
                return HttpResponsePermanentRedirect(url + '.html')
            return HttpResponsePermanentRedirect(url)
