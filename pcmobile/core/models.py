# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.db import models
import dbsettings


class Currency(models.Model):
    name = models.CharField(_(u'Name'), max_length=70)
    code = models.CharField(_(u'Code'), unique=True, max_length=10)
    abbr = models.CharField(_(u'Abbr'), max_length=10)
    format_str = models.CharField(
        _(u'Format string'),
        max_length=50, default=u'%(value).2f %(abbr)s')

    coeffitient = models.FloatField(_(u'Coeffitient'), default=1.0)

    class Meta:
        verbose_name = _(u'Currency')
        verbose_name_plural = _(u'Currencies')

    def __unicode__(self):
        return self.name

    def get_formatted_value(self, value, convert=False):
        import locale
        if not value:
            value = 0.0

        if convert:
            value *= self.coeffitient

        return locale.format_string(self.format_str, {
            'value': value,
            'abbr': self.abbr,
            'code': self.code,
            'name': self.name,
        })
CURRENCY_CHOICES = ((None, '---'),)
try:
    for currency in Currency.objects.all():
        CURRENCY_CHOICES += ((currency.code, currency.name),)
except:
    pass


class SiteOptions(dbsettings.Group):
    title = dbsettings.StringValue(_(u'Site title'))
    main_site_domain = dbsettings.StringValue(_(u'Main site domain'))
    api_entrypoint = dbsettings.StringValue(_(u'API entrypoint'))
    currency = dbsettings.Value(_(u'Currency'), choices=CURRENCY_CHOICES)
    order_comment = dbsettings.StringValue(_(u'Order comment'))
    mobile_message = dbsettings.StringValue(_(u'Mobile comment'))
site_options = SiteOptions(_(u'Site options'))
