��          �            h     i     z     �     �     �  I   �  -   �  @   &     g     v  (   {     �      �  )   �  �  �     �  '   �     �  -        <  �   K  >   �     +  '   �     �  ;   �     "  +   @  Z   l                         
                     	                                  %(app)s settings Application settings Clear cache Edit Settings Home Leave empty in order to retain old password. Provide new value to change. Models available in the %(name)s application. Please correct the error below. Please correct the errors below. Restart engine Save Settings included in the %(name)s class. Site settings Updated %(desc)s on %(location)s You don't have permission to edit values. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-02 17:22+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 %(app)s настройки Настройки приложения Очистить кеш Редактировать настройки Главная Оставьте пустым, чтобы сохранить старый пароль. Введите новое значение, чтобы изменить. Модель доступна в приложении %(name)s Пожалуйста, исправьте ошибку ниже. Пожалуйста, исправьте ошибки ниже. Перезапустить движок Сохранить Настройки включены в класс %(name)s. Настройки сайта Обновлено %(desc)s в %(location)s У Вас нет прав для редактирования этого значения. 