��    0      �  C         (     )     1  
   =     H     N     \     `     e     t     |     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                        '     .  
   C     N     ^     s     x  	     B   �     �     �                    -     3  :   ?     z  '   �  �  �     i  %   x     �     �     �     �     �     	      	  !   /	  !   Q	     s	     |	     �	  
   �	  
   �	     �	  
   �	     �	     �	     �	     �	     
      
     >
     [
     x
     �
  
   �
  &   �
     �
     �
  -   �
     -     B     O  q   ^     �  -   �            0   '     X     i  �          @            *   '            "   	   (           .       ,   #                    +   )         /              $             &                                               !              
                           0   %         -              Account Add voucher All images Apply Authorization Buy Cart Cart is empty. Catalog Checkout Confirm checkout Date Description From: Login Logout Next Number OK Payment method Previous Price Product lists Products found Properties Property Reviews Score: Search Search by properties Send order Shipping method Show product reviews Sort Status Thank you Thank you for your order. The store manager will contact you soon. There are no reviews yet. There is nothing to checkout To: Total: Type the search string here... Value Your orders Your username and password didn't match. Please try again. breadcrumbsHome in the selected category are no filters Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-10-01 16:06+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Кабинет Добавить сертификат Все фотографии Подтвердить Авторизация Купить Корзина Корзина пустая Каталог Оформление заказа Подтвердить заказ Дата Описание От: Войти Выйти Следующая Номер ОК Способ оплаты Предыдущая Цена Списки товаров Найдено товаров Характеристики Характеристика Отзывы Оценка: Поиск Подбор по параметрам Оформить заказ Способ доставки Показать отзывы о товаре Сортировка Статус Спасибо Спасибо за Ваш заказ. Менеджер магазина скоро свяжется с Вами. Отзывов нет. Рассчитываться не за что До: Итого: Введити текст для поиска... Значение Ваши заказы Ваше имя пользователя и пароль не совпадают. Пожалуйста, попробуйте еще раз. Главная в выбранной категории нет фильтров 