$(function() {
	var $categoryMenu = $('#category-menu'),
		$inputmask = $('.js-inputmask');
	
	$categoryMenu.length ? $categoryMenu.sapling(): false;

	if ($inputmask.length) {
		$inputmask.inputmask('999999', {
			placeholder: ""
		});
	}

});
$('.voucher-wrapper').on('click', '#voucher_check', function()
{
    var url = $('#voucher').attr("data");
    var voucher = $('#voucher').val();
    $.post(url, { "voucher" : voucher }, function(data) {
        $('.voucher-wrapper').html(data['html']);
        $('.total-price').html(parseFloat(data['total_price']));
    },
    "json"
    );
});
$('.voucher-wrapper').on('click', '#show_voucher', function(){

    $('#div_voucher').toggleClass('hidden');
    $('#div_voucher')[0].style.display = "";
    $('#show_voucher').remove();
});
loadShippingForm = function(form){
    $.ajax({
        type: 'POST',
        url: "/load-shipping-form-fields/",
        data: {
            form_id: form.val(),
        },
        dataType: 'json',
        success: function(data){
            $('.ajax-form-fields').html(data['html']);
        }
    });
}
$(function(){
    var $form = $('.form-control.main-shipping-form-field');
    if($form.length){
        $form.change(function(){
            loadShippingForm($form);
        })
    }
});
