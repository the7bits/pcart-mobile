from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^settings/', include('dbsettings.urls')),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^debug-toolbar/', include(debug_toolbar.urls)),
    )

urlpatterns += patterns(
    '',
    url(r'', include('core.urls')),
    url(r'', include('step_by_step.urls'))
)
