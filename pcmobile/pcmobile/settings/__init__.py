import os
mode = os.getenv('WSGI_ENV', 'development')

if mode == 'development':
    from .development import *
elif mode == 'testing':
    from .testing import *
elif mode == 'staging':
    from .staging import *
elif mode == 'production':
    from .production import *
