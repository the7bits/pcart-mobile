from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)
