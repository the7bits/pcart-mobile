from django.utils.translation import ugettext_lazy as _
import os

BASE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)),
    '..')

SECRET_KEY = '+l!-&#h2y6uv*dh=)qc7=qku#^o3dptz98=3@p*cvc3fz$hxjk'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

PCART_MOBILE_VERSION = "0.0.9"

# Application definition

INSTALLED_APPS = (
    'basic_theme',
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.redirects',
    'django_extensions',
    'mptt',
    'dbsettings',
    'core',
    'catalog',
    'compressor',
    'djcompass',
    'checkout',
    'sbits_pages',
    'rootfiles',
    'step_by_step',
    'product_lists'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'core.middleware.MaintenanceMiddleware'
)

# COMPRESS_PRECOMPILERS =  (
#     ('text/scss', 'sass --scss {infile} {outfile}'),
#     ('text/scss', 'sass --scss --compass {infile} {outfile}'),
# )

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

ROOT_URLCONF = 'pcmobile.urls'

WSGI_APPLICATION = 'pcmobile.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, 'static'),
# )

PUBLIC_DIR = os.path.join(BASE_DIR, '..', 'public')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')
STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')


COMPASS_INPUT = STATIC_ROOT  # os.path.join(STATIC_ROOT, 'sass')
COMPASS_OUTPUT = os.path.join(STATIC_ROOT, 'css')
COMPASS_STYLE = 'compact'
COMPASS_IMAGE_DIR = os.path.join(COMPASS_INPUT, 'images')
COMPASS_RELATIVE_URLS = True

LOGIN_REDIRECT_URL = '/'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    'core.context_processors.main',
)

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 60 * 60,  # 1 hour
    },
}

APPEND_SLASH = True

SUIT_CONFIG = {
    'ADMIN_NAME': ' v.'.join(['P-Cart Mobile', PCART_MOBILE_VERSION]),
    'MENU': (
        'auth',
        {
            'label': _(u'Catalog'),
            'app': 'catalog',
            'models': [
                'catalog.sorttype',
            ]
        },
        {
            'label': _(u'Core'),
            'app': 'core',
            'models': (
                'core.currency',
            )
        },
        {
            'label': _(u'Settings'),
            'icon': 'icon-cog',
            'models': (
                {
                    'url': 'site_settings',
                    'label': _(u'General')
                },
                'redirects.redirect',
                'sites.site',
            )
        },
        {
            'label': _(u'Service files'),
            'app': 'rootfiles',
            'models': (
                'rootfiles.file',
            )
        },
    ),
}

SITE_ID = 1

try:
    from server_settings import *
except:
    pass
