from django.shortcuts import render_to_response
from core.models import site_options
from django.template import RequestContext
import requests
from django.http import Http404
from catalog.models import SortType


def get_product_lists_page(
        request, template_name="product_lists/product_lists.html"):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}product-lists/'.format(
            api_entrypoint))
    if r.status_code == 404:
        raise Http404
    product_lists = r.json()
    return render_to_response(
        template_name,
        RequestContext(request, {
            'product_lists': product_lists,
        }))


def get_product_list(
        request,
        page_slug,
        template_name="product_lists/product_list_page.html"):
    api_entrypoint = site_options.api_entrypoint
    params = request.GET.copy()
    if params:
        r = requests.get(
            '%sproduct-list/%s/' % (api_entrypoint, page_slug),
            params=params)
    else:
        r = requests.get(
            '%sproduct-list/%s/' % (api_entrypoint, page_slug))
    if r.status_code == 404:
        raise Http404
    product_list = r.json()
    product_list_items = product_list['product_list_items']
    if product_list_items['next']:
        next_url = '%s?%s' % (
            request.path, product_list_items['next'].split('?')[1])
    else:
        next_url = None
    if product_list_items['previous']:
        previous_url = '%s?%s' % (
            request.path, product_list_items['previous'].split('?')[1])
    else:
        previous_url = None
    current = params['sorting'] if 'sorting' in params else ''
    return render_to_response(
        template_name,
        RequestContext(request, {
            'product_list': product_list,
            'product_list_items': product_list_items['results'],
            'next': next_url,
            'previous': previous_url,
            'count': product_list_items['count'],
            'current': current,
            'sort_types': SortType.objects.all(),
        }))
