# coding: utf-8

from __future__ import unicode_literals
from django.db import models
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator


@python_2_unicode_compatible
class File(models.Model):
    name = models.CharField(
        _('Name'),
        max_length=100,
        validators=[RegexValidator(regex=r'^.*\..*$',
            message=('You need to enter the correct file name with extension.'))]
        )
    content = models.TextField(_('Content'), blank=True)
    created_at = models.DateTimeField(_('Created'), auto_now_add=True)
    modified_at = models.DateTimeField(_('Modified'), auto_now=True)
    is_accessable = models.BooleanField(_('Accessable'), default=True)

    class Meta:
        verbose_name = _('Service file')
        verbose_name_plural = _('Service files')

    def __str__(self):
        return six.text_type(self.name)
