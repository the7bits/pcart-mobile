# coding: utf-8
from __future__ import unicode_literals
from django.contrib import admin
from rootfiles.models import File


class FileAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'modified_at', 'is_accessable')
    search_fields = ('name',)
    ordering = ('name',)
    date_hierarchy = 'modified_at'

admin.site.register(File, FileAdmin)
