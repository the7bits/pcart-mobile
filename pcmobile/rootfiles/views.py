# coding: utf-8
from __future__ import unicode_literals
from django.http import HttpResponse
import mimetypes
import time
from rootfiles.models import File
from django.shortcuts import get_object_or_404
from django.utils.http import http_date


def serve(request, filename):
    file = get_object_or_404(File, name=filename, is_accessable=True)
    mimetype, encoding = mimetypes.guess_type(file.name)
    response = HttpResponse(file.content, content_type=mimetype)
    response['Last-Modified'] = http_date(
        int(time.mktime(file.modified_at.timetuple())))
    return response
