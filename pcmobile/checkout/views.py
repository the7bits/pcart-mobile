# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.urlresolvers import reverse
from core.models import site_options
import requests
import json
from django.views.decorators.http import require_POST
from .utils import get_voucher_number
from django.template.loader import render_to_string


@ensure_csrf_cookie
def checkout(request, template_name="checkout/checkout.html"):
    api_entrypoint = site_options.api_entrypoint
    order_comment = site_options.order_comment
    mobile_message = site_options.mobile_message
    if request.POST:
        if request.user.is_anonymous():
            username = None
        else:
            username = request.user.username
        # copy request
        postData = request.POST.copy()

        # if mobile set message - rewrite message
        if mobile_message:

            if 'message' in request.POST and request.POST['message']:
                message = request.POST['message']
                message = '%s. %s' % (mobile_message, message)
            else:
                message = mobile_message

            # set message
            postData['message'] = message
        r = requests.post(
            '{0}send-order/'.format(api_entrypoint),
            data={
                'order_data': json.dumps(dict(postData)),
                'mobile_session': request.session.get(
                    'mobile_session', None),
                'username': username,
            })
        result = r.json()
        if 'order' in result and result['order']:
            session = request.session
            session['order'] = result['order']
        if 'status' in result and result['status'] == 'ok':
            return HttpResponseRedirect(reverse('thank_you_page'))
    else:
        # if request.user.is_anonymous():
        #     return HttpResponseRedirect(reverse('login'))
        if request.user.is_anonymous():
            username = None
        else:
            username = request.user.username
        r = requests.post(
            '{0}get-checkout-fields/'.format(api_entrypoint),
            data={'mobile_session': request.session.get(
                'mobile_session', None), 'username': username}
        )
        result = r.json()
    total_price = 0
    for item in result['items']:
        item['product']['total_price'] = item['product']['price'] \
            * item['amount']
        total_price += item['product']['price'] * item['amount']
    data = {
        'fields': result['fields'],
        'items': result['items'],
        'total_price': total_price,
        'payment_methods': result['payment_methods'],
        'shipping_methods': result['shipping_methods'],
        'order_comment': order_comment,
        }
    if 'shipping_form_fields' in result:
        data.update({'shipping_form_fields': result['shipping_form_fields']})
    voucher_number = get_voucher_number(request)
    r = requests.post(
        '{0}voucher/'.format(api_entrypoint),
        data={
            'voucher_number': voucher_number,
            'mobile_session': request.session.get(
                'mobile_session', None)
        })
    result = r.json()
    if result['success']:
        result.pop('items')
        data.update(result)
        data['total_price'] -= result['voucher_value']
    return render_to_response(
        template_name,
        RequestContext(request, data))


def thank_you(request, template_name='checkout/thank_you.html'):
    order = request.session.get('order', {}).copy()
    if 'order' in request.session:
        del request.session['order']
    return render_to_response(
        template_name,
        RequestContext(request, {'order': order}))


@require_POST
def check_voucher(request):
    api_entrypoint = site_options.api_entrypoint
    voucher_number = get_voucher_number(request)
    request.session["voucher_number"] = voucher_number
    r = requests.post(
        '{0}voucher/'.format(api_entrypoint),
        data={
            'voucher_number': voucher_number,
            'mobile_session': request.session.get(
                'mobile_session', None)
        })
    result = r.json()
    total_price = 0
    for item in result['items']:
        item['product']['total_price'] = item['product']['price'] \
            * item['amount']
        total_price += item['product']['price'] * item['amount']
    if result['success']:
        total_price -= result['voucher_value']
    html = render_to_string(
        "checkout/voucher.html",
        RequestContext(request, result)
        )
    return HttpResponse(json.dumps({
        'html': html,
        'total_price': total_price,
        }))


@require_POST
def load_shipping_form_fields(request):
    api_entrypoint = site_options.api_entrypoint
    r = requests.post(
        '{0}shipping-form-fields/'.format(api_entrypoint),
        data={
            'shipping_method': request.POST['form_id'],
        })
    result = r.json()
    html = render_to_string(
        'checkout/shipping_form_fields.html',
        RequestContext(request, result)
        )
    return HttpResponse(json.dumps({
        'html': html
        }))
