def get_voucher_number(request):
    return request.POST.get(
        "voucher", request.session.get("voucher_number", ""))
