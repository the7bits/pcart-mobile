# coding: utf-8
from django import template
import requests
from core.models import site_options


register = template.Library()


@register.simple_tag(takes_context=True)
def cart_items_count(context):
    api_entrypoint = site_options.api_entrypoint
    request = context.get('request')
    if 'mobile_session' in request.session:
        r = requests.post(
            '{0}get-cart/'.format(api_entrypoint),
            data={'mobile_session': request.session['mobile_session']}
        )
        cart_items = r.json()
        if 'status' in cart_items and cart_items['status'] == u'no session':
            return ''
        amount = 0
        for item in cart_items:
            amount += item['amount']
        return int(amount)
    else:
        return ''
