# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
import requests
from core.models import site_options
from django.views.decorators.http import require_POST
from django.core.urlresolvers import reverse
import json


@require_POST
def buy_product(request):
    api_entrypoint = site_options.api_entrypoint
    products = request.POST.getlist('products')
    product_id = request.POST.get('product_id')
    data = {}
    if 'mobile_session' in request.session:
        data['mobile_session'] = request.session['mobile_session']
    items = []
    for id in products:
        amount = request.POST.get('product_'+id)
        if amount:
            items+=[{
                'product_id': id,
                'amount': amount
            },]
    if product_id:
        amount = request.POST.get('amount', 1)
        items+=[{
            'product_id': product_id,
            'amount': amount
        },]
    data['items'] = json.dumps(items)
    r = requests.post(
        '{0}buy-product/'.format(api_entrypoint),
        data=data
    )
    result = r.json()
    request.session['mobile_session'] = result['mobile_session']
    return HttpResponseRedirect(reverse('cart'))


@ensure_csrf_cookie
def cart_view(request, template_name='cart/cart.html'):
    api_entrypoint = site_options.api_entrypoint
    if 'mobile_session' in request.session:
        r = requests.post(
            '{0}get-cart/'.format(api_entrypoint),
            data={'mobile_session': request.session['mobile_session']}
        )
        cart_items = r.json()
        if 'status' in cart_items and cart_items['status'] == u'no session':
            cart_items = []
            # total_price = None
        total_price = 0
        for item in cart_items:
            item['product']['total_price'] = item['product']['price']\
                * item['amount']
            total_price += item['product']['price'] * item['amount']
        # import pdb
        # pdb.set_trace()
    else:
        cart_items = None
        total_price = None
    return render_to_response(
        template_name,
        RequestContext(request, {
            'items': cart_items,
            'total_price': total_price,
        }))


@require_POST
def remove_product(request):
    api_entrypoint = site_options.api_entrypoint
    cart_item_id = request.POST['cart_item_id']
    if 'mobile_session' in request.session:
        requests.post(
            '{0}remove-product-from-order/'.format(api_entrypoint),
            data={'cart_item_id': int(cart_item_id)})
    return HttpResponse(json.dumps({'status': 'ok'}))


@require_POST
def change_cart_item_amount(request):
    api_entrypoint = site_options.api_entrypoint
    cart_item_id = request.POST['cart_item_id']
    amount = request.POST['amount']
    if 'mobile_session' in request.session:
        requests.post(
            '{0}change-cart-item-amount/'.format(api_entrypoint),
            data={
                'cart_item_id': int(cart_item_id),
                'amount': amount
            })
    return HttpResponse(json.dumps({'status': 'ok'}))
