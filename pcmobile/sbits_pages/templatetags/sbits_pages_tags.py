from django import template
import requests
from core.models import site_options
import re

register = template.Library()


@register.inclusion_tag('sbits_pages/category_tree.html', takes_context=True)
def show_sbits_pages_tree(context):
    return context


@register.assignment_tag
def sbits_pages_catalog_tree():
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}sbits-pages-categories/'.format(
            api_entrypoint))
    if r.status_code == 404:
        return

    categories = r.json()
    return {
        'categories': categories['categories'],
        'info_page_name': categories['info_page_name'],
    }


@register.filter()
def load_images(text):
    shop_domain = site_options.main_site_domain
    image_regex = re.compile('src="(.+?)"')
    links = image_regex.findall(text)
    new_links = []
    for link in links:
        if link.startswith('/'):
            new_links.append({
                'new': shop_domain + link,
                'old': link
            })
    for link in new_links:
        text = text.replace(link['old'], link['new'])
    return text
