from django.shortcuts import render_to_response
from core.models import site_options
from django.template import RequestContext
import requests
from django.http import Http404
# Create your views here.


def sbits_pages_categories_view(
        request, template_name='sbits_pages/categories_list.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}sbits-pages-categories/'.format(
            api_entrypoint))
    if r.status_code == 404:
        raise Http404
    categories = r.json()
    return render_to_response(
        template_name,
        RequestContext(request, {
            'sp_categories': categories,
        }))


def sbits_pages_category_view(
        request, category_slug, template_name='sbits_pages/category.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}sbits-pages-category/{1}/'.format(
            api_entrypoint, category_slug))
    if r.status_code == 404:
        raise Http404
    category = r.json()
    path = request.path.split('/')[2]
    return render_to_response(
        template_name,
        RequestContext(request, {
            'sp_category': category,
            'path': path,
        }))


def sbits_pages_news_view(
        request, path, news_slug, template_name='sbits_pages/news.html'):
    api_entrypoint = site_options.api_entrypoint
    r = requests.get(
        '{0}sbits-pages-news/{1}/'.format(
            api_entrypoint, news_slug))
    if r.status_code == 404:
        raise Http404
    news = r.json()
    sp_category = news['category'][0]
    return render_to_response(
        template_name,
        RequestContext(request, {
            'news': news,
            'sp_category_news': sp_category,
        }))
