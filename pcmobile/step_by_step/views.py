from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse, Http404
from core.models import site_options
import json
import requests


def portlet_form(request, portlet_id):
    api_entrypoint = site_options.api_entrypoint
    if request.POST:
        selects = request.POST.get('selects')
        r = requests.post(
            '{0}stepbystep/change-form'.format(api_entrypoint),
            data={'selects': selects, 'portlet_id': portlet_id})
        if r.status_code == 500:
            return Http404
        portlet_data = r.json()
    else:
        r = requests.post(
            '{0}stepbystep/portlet-detail'.format(api_entrypoint),
            data={'portlet_id': portlet_id}
        )
        if r.status_code == 500:
            return Http404
        portlet_data = r.json()
    for i in range(len(portlet_data['selects'])):
        if not portlet_data['selects'][i]['options']:
            continue
        portlet_data['selects'][i]['options'] = sorted(
            portlet_data['selects'][i]['options'].items(),
            key=lambda x: x[1]
        )
    form = render_to_string(
        'stepbystep/stepbystep_form.html',
        RequestContext(request, {
            'selects': portlet_data['selects'],
            'title': portlet_data['title'],
            'portlet_id': portlet_id
        })
    )
    if request.POST:
        return HttpResponse(json.dumps({
            'form': form,
            'url': portlet_data['url'] if 'url' in portlet_data else ''
            })
        )
    else:
        return render_to_response(
            'stepbystep/stepbystep_page.html',
            RequestContext(request, {
                'form': form,
                'portlet_id': portlet_id
            })
        )
