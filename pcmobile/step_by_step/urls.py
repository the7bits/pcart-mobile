from django.conf.urls import patterns, url


urlpatterns = patterns(
    'step_by_step.views',
    url(
        r'^step-by-step-form/(?P<portlet_id>[-\d]+)/$',
        'portlet_form',
        name='stepbystep_portlet_form'
    ),
)
