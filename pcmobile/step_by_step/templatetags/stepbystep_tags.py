# -*- coding: utf-8 -*-

from django.template import Library
from core.models import site_options
import requests


register = Library()
api_entrypoint = site_options.api_entrypoint


@register.assignment_tag
def stepbystep_portlet_list():
    r = requests.get(
        '{0}stepbystep/portlet-list'.format(api_entrypoint))
    if r.status_code == 500:
        return {}
    data = r.json()
    return data


@register.inclusion_tag(
    'stepbystep/stepbystep_car_block.html', takes_context=True)
def stepbystep_car_block(context):
    request = context.get('request')
    category = context.get('category')
    if 'car' in request.GET:
        r = requests.get('{0}stepbystep/get-car-info/{1}'.format(
            api_entrypoint, request.GET['car']),
            params={'category_slug': category['slug']}
        )
        if r.status_code == 500:
            return {}
        data = r.json()
        urls = {}
        for d in data['urls']:
            if (d['filter_name__order'], d['filter_name__name']) in urls:
                urls[(d['filter_name__order'], d['filter_name__name'])].append(
                    d)
            else:
                urls[(d['filter_name__order'], d['filter_name__name'])] = [d]
        sorted(urls)
        data['urls'] = urls
        context.update(data)
        return context
    else:
        return {}
