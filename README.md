Разворачивание мобильной версии.
Установить от sudo
```
#!python
sudo apt-get install ruby1.9.1-dev
sudo gem install compass --pre
```
Если разворачивать будем на grape'e, переключаемся на юзера app
```
#!python
 su -l app
```
Дальше генерируем ssh ключ для юзера app и добавляем его к этому репозиторию
```
#!python
ssh-keygen -t dsa
```
Сгенерированый ключ лежит в ```/home/app/.ssh/id_dsa.pub```

Дальше создаем папку tmp в ```/home/app/webapp``` в нее клонируем репозиторий мобильной версии.
Создаем папки ```/home/app/webapp/public/static``` ```/home/app/webapp/public/media```
Дальше копируем папку ```/home/app/webapp/tmp/pcart-mobile/pcmobile``` в ```/home/app/webapp```.

Дальше нужно настрить проект, указать в production.py настройки базы и статики\медии
```
#!python
STATIC_ROOT = '/home/app/webapp/public/static'
MEDIA_ROOT = '/home/app/webapp/public/media'
```
Выключить дебаг режим и укзать 
```
#!python
ALLOWED_HOSTS = ['*']
```

Идем в файл ```/home/app/webapp/passenger_wsgi.py```
Коментируем все что выше строчки 
```Uncomment the next lines for serving tha Django project```
и раскоментируем все что ниже и добавляем 
```
#!python
os.environ['WSGI_ENV'] = 'production'
```
Проверяем все пути, и меняем название проекта
